"""
Example script for population download.
Data source:  https://population.un.org/wpp/Download/Files/1_Indicators%20(Standard)/CSV_FILES/WPP2019_TotalPopulationBySex.csv


"""

import pandas as pd
import requests
import country_converter as coco
from pathlib import Path


def download_pop_data(src_url, storage_path, force_download=False):
    """Store the population dataset at storage path

    Parameters
    ----------

    src_url: str
        Url of the source data

    storage_path: pathlib.Path
        Location for storing the data

    force_download: boolean, optional
        If True, downloads the data even if it is already present in storage_path.
        If False (default), only downloads the data if is is not available locally.

    Returns
    -------
        Downloaded File: pathlib.Path

       """
    filename = Path(src_url.split("/")[-1])

    # Making the storage path if should not exisit
    storage_path.mkdir(parents=True, exist_ok=True)
    storage_file = storage_path / filename
    if storage_file.exists() and (force_download is False):
        return storage_file
    download = requests.get(src_url)

    # Raise exception if the file is not available
    download.raise_for_status()
    with open(storage_file, "wb") as sf:
        sf.write(download.content)

    return storage_file



def read_pop_data(data_file, relevant_years=None):
    """Reads the data and returns dataframe

    Parameters
    ----------

    data_file: pathlib.Path
        Extracted fao csv file

    relevant_years: list(str)
        Relevant years defined in the main : relevant_years = list(range(1995,2021))
        Select the years of interest, in this case from 1995 to 2020

    """
    df = pd.read_csv(data_file, encoding="latin-1")
    df = df[df.columns[~df.columns.isin(['VarID','MidPeriod'])]]
    df = df[(df.Variant == 'Medium') & (df.Time.isin(relevant_years))]
    country_code = list(df['LocID'])
    converter=coco.country_converter
    df['ISO3']=converter.convert(names = country_code, to='ISO3')
    df = df[(df.ISO3 != 'not found')]
    meta_col = [col for col in df.columns]
    return df[meta_col]
  


def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)


def get_missing_data(df):
    """Make a summary table with all missing data

    Any row with a nan is included in the resulting table

    fao_df: pandas DataFrame
        Based on raw csv read

    """
    return df[df.isnull().any(1)]


def main():

    src_url = "https://population.un.org/wpp/Download/Files/1_Indicators%20(Standard)/CSV_FILES/WPP2019_TotalPopulationBySex.csv"
    src_csv = Path("WPP2019_TotalPopulationBySex.csv")

    storage_root = Path("./population").absolute()
    download_path = storage_root / "download"
    data_path = storage_root / "download"

    relevant_years = list(range(1995,2021))
    
    data_pop_csv = download_pop_data(src_url=src_url, storage_path=download_path)

    pop_all = read_pop_data(data_path / src_csv, relevant_years=relevant_years)
    pop_missing = get_missing_data(pop_all)

    pop_all.to_csv(data_path / 'refresh_population.csv')
    pop_missing.to_csv(data_path / 'missing_data.csv')

    return locals()


if __name__ == "__main__":


    locals().update(main())

