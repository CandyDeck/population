Example script for population download.
Data source:  https://population.un.org/wpp/Download/Files/1_Indicators%20(Standard)/CSV_FILES/WPP2019_TotalPopulationBySex.csv

Source downloaded on the 8th of February 2021.

The initial .csv file downloaded is encoded in UTF-8 and has the following columns:

- LocID (numeric): numeric code for the location; for countries and areas, it follows the ISO 3166-1 numeric standard
- Location (string): name of the region, subregion, country or area
- VarID (numeric): numeric code for the variant
- Variant (string): projection variant name (Medium is the most used); for more information see Definition of projection variants
- Time (string): label identifying the single year (e.g. 1950) or the period of the data (e.g. 1950-1955)
- MidPeriod (numeric): numeric value identifying the mid period of the data, with the decimal representing the month (e.g. 1950.5 for July 1950)
- PopMale: Total male population (thousands), annually from 1950 to 2100
- PopFemale: Total female population (thousands), annually from 1950 to 2100
- PopTotal: Total population, both sexes (thousands), annually from 1950 to 2100
- PopDensity: Population per square kilometre (thousands), annually from 1950 to 2100


In **def main()**, it is possible to select the relevant years, which are the years of interest in our/your project. Here, we are focusing on data from **1995** to **2020**.

2 outputs are exported into .csv files :


- **refresh population** : this new table has the following columns :  

    - LocID (numeric): numeric code for the location; for countries and areas, it follows the ISO 3166-1 numeric standard
    - Location (string): name of the region, subregion, country or area
    - Variant : Medium, which is the most used
    - Time (string): label identifying the single year or the period of the data of interest (e.g. 1995-2020)
    - PopMale: Total male population (thousands), annually for 1995-2020
    - PopFemale: Total female population (thousands), annually for 1995-2020
    - PopTotal: Total population, both sexes (thousands), annually for 1995-2020
    - PopDensity: Population per square kilometre (thousands), annually for 1995-2020
    - ISO3 : Country code

- **missing data** : this is listing the data for which at least 1 total population input is missing.
